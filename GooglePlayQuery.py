#
# little simple app to query google play market
#
# 2013 - spraysoft
#
import pycurl
import sys
import requests
import cStringIO
from HTMLParser import HTMLParser
from lxml import html
from lxml import etree

CARD_CLASS="card-content"

def print_space(nn):
  for i in range(0,nn):
    print " ",


counter = 1

def handle_card_content(elem):
  global counter
  hlink = None
  for child in elem:
    klass = get_element_klass(child)
    if klass == "cover":
      continue
    elif klass == "details":
      subch = child.find("a")
      if subch != None:
        ar = subch.get("href").split("?")
        if len(ar) > 1:
          ar2 = ar[1].split("=")
          if len(ar) > 1:
            hlink = ar2[1]

  if hlink != None:
    counter = counter + 1
    print "[%03d] LINK: '%s'"%(counter,hlink)

def get_element_klass(child,filter=None):
  klass = None
  for at in child.attrib:
    if at == "class":
      klass = child.attrib[at]
      break

  if filter != None and klass != None:
    sp = klass.split(" ")
    if filter in sp:
      return klass
    return None
    
  return klass

def recurse_on_child(child,lev=0):  
  klass = None

  klass = get_element_klass(child, CARD_CLASS)

  if klass != None:
    handle_card_content(child)

  else:
    for c in child:
        recurse_on_child(c,lev+1)

def retrieveData(query="simon",n_entries=250,category="apps"):
  r=requests.post('https://play.google.com/store/search', 
                  data={ "ipf": 1, "num": n_entries, "numChildren": 0, "xdr": 1, "start": 0 },
                  params={ "q": query, "c": category } )
  tree = html.fromstring(r.content)
  for child in tree:
    recurse_on_child(child)

if __name__ == '__main__':
  if len(sys.argv) < 2: 
    print "Please give a string to query"
    sys.exit()

  q=sys.argv[1]
  print "Querying '%s'"%q
  retrieveData(query=q)

