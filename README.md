Google Play Query Silly App
=======================

I often have the need to see on which place my apps are listed on queries
made to Google Play market.

I used to do the query manually, but I wanted to do it automatically.

Performing some search, I didnt find any tool to perform the query (without
using any google account), so I written this simple python script.

Example usage:

    python GooglePlayQuery.py simon | grep spraysoft

    [108] LINK: 'com.spraysoft.Memofart'


And I know that my app Simon's Fart (package name com.spraysoft.Memofart) is at 108 in 
the query.


Check out my [applications](http://goo.gl/CdhAHz) on google play market.


Please note that this project is not affiliated, connected or associated in any way with Google.


# Dependencies

To run GooglePlayQuery you'll need [requests](http://docs.python-requests.org/en/latest/ Requests: HTTP for Humans) 
and [lxml](http://lxml.de/  XML and HTML with Python) modules on your python installation.

---
2013 - spraysoft
